from django.shortcuts import render, redirect, get_object_or_404
from laboratorio.models import *
from laboratorio.forms import MainForm

# Create your views here.

def index(request):
    return render(request, 'index.html')

def create(request):
    if request.method == 'POST':
        form = MainForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('read')
    else:
        form = MainForm()
    return render(request, 'create.html', {'form': form})

views = 0

def read(request):
    labs = Laboratorio.objects.order_by('id')
    global views
    views += 1
    return render(request, 'read.html', {'labs': labs, 'views': views})

def update(request, id):
    lab = get_object_or_404(Laboratorio, pk=id)
    if request.method == 'POST':
        form = MainForm(request.POST, isinstance=lab)
        if form.is_valid():
            form.save()
            return redirect('read')
    else:
        form = MainForm(instance=lab)
    return render(request, 'update.html', {'form': form})

def delete(request, id):
    lab = get_object_or_404(Laboratorio, pk=id)
    return render(request, 'delete.html', {'lab': lab})

def proceed_deletion(request, id):
    lab = get_object_or_404(Laboratorio, pk=id)
    if lab:
        lab.delete()
    return redirect('read')