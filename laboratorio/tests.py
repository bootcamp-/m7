from django.test import TestCase
from django.urls import reverse

# Create your tests here.

from laboratorio.models import Laboratorio

class Test(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.laboratorio = Laboratorio.objects.create(nombre='LaboratorioTest', ciudad='CiudadTest', pais='PaisTest')

    def test_1(self):
        self.assertEqual(self.laboratorio.nombre, 'LaboratorioTest')
        self.assertEqual(self.laboratorio.ciudad, 'CiudadTest')
        self.assertEqual(self.laboratorio.pais, 'PaisTest')

    def test_2(self):
        index = self.client.get('/')
        self.assertEqual(index.status_code, 200)

    def test_3(self):
        index = self.client.get(reverse('index'))
        self.assertEqual(index.status_code, 200)
        self.assertTemplateUsed(index, "index.html")
        self.assertContains(index, "Bienvenido")