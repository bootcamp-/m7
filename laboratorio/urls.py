from django.urls import path
from laboratorio.views import index, create, read, update, delete, proceed_deletion

urlpatterns = [
    path('', index, name='index'),
    path('crear/', create, name='create'),
    path('ver/', read, name='read'),
    path('actualizar/<int:id>', update, name='update'),
    path('eliminar/<int:id>', delete, name='delete'),
    path('continuar_eliminacion/<int:id>', proceed_deletion, name='proceed_deletion'),
]