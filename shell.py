# ejecutar desde consola con: py manage.py shell < shell.py

from laboratorio.models import Laboratorio, DirectorGeneral, Producto

# todos los objetos de Laboratorio, DirectorGeneral, Producto
laboratorios = Laboratorio.objects.all()
directores = DirectorGeneral.objects.all()
productos = Producto.objects.all()

print(laboratorios, directores, productos, sep='\n')

# laboratorio del 'Producto 1'
producto = Producto.objects.get(nombre='Producto 1')
laboratorio_producto1 = producto.laboratorio
print(laboratorio_producto1)

# productos ordenados por nombre, mostrando nombre y laboratorio
productos_en_orden = Producto.objects.order_by('nombre')
for i in productos_en_orden: print(f"{i.nombre}, {i.laboratorio}")

# imprimir laboratorios de todos los productos
for i in productos: print(f"{i.laboratorio}")